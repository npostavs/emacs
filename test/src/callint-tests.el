;;; callint-tests.el --- unit tests for callint.c    -*- lexical-binding: t; -*-

;; Copyright (C) 2018-2020 Free Software Foundation, Inc.

;; Author: Philipp Stephani <phst@google.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Unit tests for src/callint.c.

;;; Code:

(require 'ert)
(require 'bytecomp)

(ert-deftest call-interactively/incomplete-multibyte-sequence ()
  "Check that Bug#30004 is fixed."
  (let ((data (should-error (call-interactively (lambda () (interactive "\xFF"))))))
    (should
     (equal
      (cdr data)
      '("Invalid control letter `\u00FF' (#o377, #x00ff) in interactive calling string")))))

(ert-deftest call-interactively/embedded-nulls ()
  "Check that Bug#30005 is fixed."
  (should (equal (let ((unread-command-events '(?a ?b)))
                   (call-interactively (lambda (a b)
                                         (interactive "ka\0a: \nkb: ")
                                         (list a b))))
                 '("a" "b"))))

;; NOTE: We're purposely checking the dynamic value of the symbol
;; `callint-tests-x' (it should be void), which names a *lexical*
;; variable.

(ert-deftest callint-interactive-form-binding-1 ()
  "Test that interactive form evaluation uses lexical binding."
  (let ((fun
         (lambda (a b)
           (interactive
            (list (bound-and-true-p callint-tests-x)
                  (funcall (let* ((callint-tests-x 42))
                             (lambda () callint-tests-x)))))
           (list a b))))
    (ert-info ("interp")
      (should (equal (call-interactively fun)
                     '(nil 42))))
    (setq fun (byte-compile fun))
    (ert-info ("compiled")
      (should (equal (call-interactively fun)
                    '(nil 42))))))

(ert-deftest callint-interactive-form-binding-2 ()
  "Test that interactive form evaluation uses lexical binding."
  ;; Outer lexical environment is not preserved inside interactive
  ;; forms, so this test fails.
  :expected-result :failed
  (let ((fun
         (let ((callint-tests-x 42))
           (lambda (a b)
             (interactive (list (bound-and-true-p callint-tests-x)
                                callint-tests-x))
             (list a b callint-tests-x)))))
    (ert-info ("interp")
      (should (equal (call-interactively fun)
                     '(nil 42 42))))
    (ert-info ("compiled")
      (should (equal (call-interactively (byte-compile fun))
                    '(nil 42 42))))))

(ert-deftest call-interactively-prune-command-history ()
  "Check that Bug#31211 is fixed."
  (let ((history-length 1)
        (command-history ()))
    (dotimes (_ (1+ history-length))
      (call-interactively #'ignore t))
    (should (= (length command-history) history-length))))

;;; callint-tests.el ends here
